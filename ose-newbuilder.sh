#!/bin/bash

./build-builder.sh

CURRENT_DIR="$(readlink -f $(dirname $(readlink -f $0)))"
[ -r $CURRENT_DIR/ose-config.sh ] && . $CURRENT_DIR/ose-config.sh
[ -r $CURRENT_DIR/ose-common.sh ] && . $CURRENT_DIR/ose-common.sh

osePushNewBuilder
oseStartBuild
