package de.dydx.spring;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Main
 *
 * @author rschamm
 */
@SpringBootApplication
public class Main {
	public static void main(String[] args) {
		new SpringApplicationBuilder()
			.sources(Main.class)
			.run(args);
	}
}
