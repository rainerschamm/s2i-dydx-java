node('maven') {
   // define commands
   def ocCmd = "oc --token=`cat /var/run/secrets/kubernetes.io/serviceaccount/token` --server=https://openshift.default.svc.cluster.local --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt"
   def mvnCmd = "mvn -s ../localm2/cicd-settings.xml"

   git branch: 'master', url: 'http://gogs.cicd.svc:3000/gogs/s2i-dydx-java.git'
//   sh "GIT_COMMITTER_NAME=jenkinsslave GIT_COMMITTER_EMAIL=jenkinsslave@fun.com git clone http://gogs.cicd.svc:3000/gogs/s2i-dydx-java.git"
   def v = version()

   stage 'Build'
   dir ('source') {
		sh "${mvnCmd} clean install -DskipTests=true"
   }

   stage 'Test and Analysis'
   dir ('source') {
		echo "\n\n\n*** Running tests ***"
		sh "${mvnCmd} test"
		step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])

		echo "\n\n\n*** Running analysis ***"
		sh "${mvnCmd} jacoco:report sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -DskipTests=true"
   }

   stage 'Push to Nexus'
   dir ('source') {
		sh "${mvnCmd} deploy -DskipTests=true"
   }

   stage 'Deploy DEV'
   sh "${ocCmd} start-build java1 --from-file=source/target/hello-${v}.jar --wait=true -n dev"

   stage 'Deploy STAGE'
   input message: "Promote to STAGE?", ok: "Promote"
   sh "${ocCmd} tag dev/java1:latest stage/java1:${v} -n stage"

   sh "curl -s -L -O https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 && chmod a+x ./jq-linux64 && mv ./jq-linux64 ./jq"
   def jqFilter = ".spec.triggers[1].imageChangeParams.from.name = \"java1:${v}\""
   sh "${ocCmd} export dc java1 --output=json -n stage | ./jq '${jqFilter}' | ${ocCmd} replace dc java1 -n stage -f -"

}

def version() {
	def matcher = readFile('source/pom.xml') =~ '<version>(.+)</version>'
	matcher ? matcher[1][1] : null
}
