#!/bin/bash

CURRENT_DIR="$(readlink -f $(dirname $(readlink -f $0)))"
[ -r $CURRENT_DIR/ose-config.sh ] && . $CURRENT_DIR/ose-config.sh
[ -r $CURRENT_DIR/ose-common.sh ] && . $CURRENT_DIR/ose-common.sh

oc tag $OSE_PROJECT/$OSE_APP_IMAGE:latest $PROMOTED_PROJECT/$OSE_APP_IMAGE:$ORIGINAL_VERSION
oc new-app $OSE_APP_IMAGE:$ORIGINAL_VERSION -n $PROMOTED_PROJECT
oc expose svc/$OSE_APP_IMAGE -n $PROMOTED_PROJECT
