#
# Initial setup
#

oc new-project dev --display-name="Dev"
oc new-project stage --display-name="Stage"


cd s2i-dydx-java/
./ose-newapp.sh 
./ose-setup-stage.sh 


#
# Create CI/CD project
#

oc new-project cicd --display-name="CI/CD"
oc policy add-role-to-user edit system:serviceaccount:cicd:default -n dev
oc policy add-role-to-user edit system:serviceaccount:cicd:default -n stage


#
# Jenkins
# 

oc create -f jenkins-claim-1.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 1/*
sudo chown -R 1001:0 1
sudo chmod 777 1

# pull
docker pull registry.access.redhat.com/openshift3/jenkins-2-rhel7:latest
docker pull registry.access.redhat.com/openshift3/jenkins-slave-maven-rhel7:latest
docker tag registry.access.redhat.com/openshift3/jenkins-2-rhel7 openshift3/jenkins-2-rhel7

oc process -f jenkins-template.yaml | oc create -f -

#
# START: Only required if jenkins needs to run as root
#
oc create serviceaccount jenkins
oc adm policy add-scc-to-user anyuid -z jenkins
oc create -f jenkins-rb.yaml
oc edit dc jenkins

Add this under: spec->template->spec
...
                "serviceAccountName": "jenkins",
                "serviceAccount": "jenkins",
                "securityContext": {
                    "runAsUser": 0
                },
...
#
# END: Steps for running as root
#

# Wait for the jenkins pod to finish starting up.

# Now create a new pipeline job with this simple maven build:
# Creds are admin:password

...
node('maven') {
   // define commands
   def ocCmd = "oc --token=`cat /var/run/secrets/kubernetes.io/serviceaccount/token` --server=https://openshift.default.svc.cluster.local --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt"
   def mvnCmd = "mvn"

   git branch: 'master', url: 'https://bitbucket.org/rainerschamm/s2i-dydx-java.git'
   def v = version()

   stage 'Build'
   dir ('source') {
		sh "${mvnCmd} clean"
   }
}

def version() {
	def matcher = readFile('source/pom.xml') =~ '<version>(.+)</version>'
	matcher ? matcher[1][1] : null
}
...

# Perform a test build from jenkins UI.


#
# Nexus
#

We will use the following image: https://hub.docker.com/r/sonatype/nexus/
Default credentials are: admin / admin123

oc create -f nexus-claim-1.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 2/*
sudo chown -R 200:200 2
sudo chmod 777 2

docker pull sonatype/nexus
oc new-app sonatype/nexus
oc expose svc nexus
oc volume deploymentconfigs/nexus --add --name=nexus-volume-1 -t pvc --claim-name=nexus-claim-1 --overwrite
oc env dc/nexus CONTEXT_PATH="/"

# Now login as admin and fix the "Base URL" in the settings as well

For more info see:
https://books.sonatype.com/nexus-book/reference/install-sect-proxy.html
https://books.sonatype.com/nexus-book/reference/configxn-sect-customizing-server.html#fig-config-administration-application-server

# Fix this line in the pipeline script and make sure that its using nexus now as a maven mirror:

...
   def mvnCmd = "mvn -s ../localm2/cicd-settings.xml"
...


#
# Sonarqube
#

oc create -f postgresql-claim-1.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 6/*
sudo chown -R 26:0 6
sudo chmod 777 6

oc new-app -e POSTGRESQL_USER=sonar -e POSTGRESQL_PASSWORD=sonar -e POSTGRESQL_DATABASE=sonar registry.access.redhat.com/rhscl/postgresql-94-rhel7 --name=postgresql-sonar
oc volume deploymentconfigs/postgresql-sonar --add --name=postgresql-sonar-volume-1 -t pvc --claim-name=postgresql-claim-1 --overwrite

# Make sure the postgresql pod is fully started up

We will be using this image:
https://hub.docker.com/_/sonarqube/

oc create -f sonarqube-claim-1.yaml 
oc create -f sonarqube-claim-2.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 3/*
sudo rm -rf 4/*
sudo chown -R root:root 3
sudo chown -R root:root 4
sudo chmod 777 3
sudo chmod 777 4

docker pull sonarqube
oc new-app sonarqube
oc expose svc sonarqube

# Spin down sonar to 0 pods

oc volume deploymentconfigs/sonarqube --add --name=sonarqube-volume-1 -t pvc --claim-name=sonarqube-claim-1 --overwrite
oc volume deploymentconfigs/sonarqube --add --name=sonarqube-volume-2 -t pvc --claim-name=sonarqube-claim-2 --overwrite

oc env dc/sonarqube SONARQUBE_JDBC_USERNAME=sonar
oc env dc/sonarqube SONARQUBE_JDBC_PASSWORD=sonar
oc env dc/sonarqube SONARQUBE_JDBC_URL=jdbc:postgresql://postgresql-sonar/sonar

# Spin up sonar to 1 pod again

Here is the admin guide:
http://docs.sonarqube.org/display/SONAR/Administration+Guide

# Update the pipeline to do static tests and static analysis and push to nexus

...
   stage 'Build'
   dir ('source') {
		sh "${mvnCmd} clean install -DskipTests=true"
   }

   stage 'Test and Analysis'
   dir ('source') {
		echo "\n\n\n*** Running tests ***"
		sh "${mvnCmd} test"
		step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])

		echo "\n\n\n*** Running analysis ***"
		sh "${mvnCmd} jacoco:report sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -DskipTests=true"
   }

   stage 'Push to Nexus'
   dir ('source') {
		sh "${mvnCmd} deploy -DskipTests=true"
   }
...


#
# Gogs
#

oc create -f postgresql-claim-2.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 1/*
sudo chown -R 26:0 1
sudo chmod 777 1

oc new-app -e POSTGRESQL_USER=gogs -e POSTGRESQL_PASSWORD=gogs -e POSTGRESQL_DATABASE=gogs registry.access.redhat.com/rhscl/postgresql-94-rhel7 --name=postgresql-gogs
oc volume deploymentconfigs/postgresql-gogs --add --name=postgresql-gogs-volume-1 -t pvc --claim-name=postgresql-claim-2 --overwrite

# Make sure the postgresql pod is fully started up

We will be using the following image:
https://hub.docker.com/r/gogs/gogs/
https://github.com/gogits/gogs/tree/master/docker

oc create -f gogs-claim-1.yaml 
oc get pv # and look which pv was assigned to claim

cd /var/nfsshare/
sudo rm -rf 7/*
sudo chown -R 1000:0 7
sudo chmod 777 7

docker pull gogs/gogs
oc new-app gogs/gogs
oc expose svc gogs
oc edit route gogs -o json # and fix the http port from 22 to 3000
oc volume deploymentconfigs/gogs --add --name=gogs-volume-1 -t pvc --claim-name=gogs-claim-1 --overwrite


# Finish initialization from browser using this config:

Database Configuration:

Type
    postgres
Host
    postgresql-gogs:5432
Name
    gogs
User
    gogs
Password:
    gogs


# Sign up and login as:
User: gogs
Password: gogs

# Perform a new migraton for:

url: https://rainerschamm@bitbucket.org/rainerschamm/s2i-dydx-java.git
and repo name: s2i-dydx-java

# Use this pipeline:

...
node('maven') {
   // define commands
   def ocCmd = "oc --token=`cat /var/run/secrets/kubernetes.io/serviceaccount/token` --server=https://openshift.default.svc.cluster.local --certificate-authority=/run/secrets/kubernetes.io/serviceaccount/ca.crt"
   def mvnCmd = "mvn -s ../localm2/cicd-settings.xml"

   git branch: 'master', url: 'http://gogs:3000/gogs/s2i-dydx-java.git'
   def v = version()

   stage 'Build'
   dir ('source') {
		sh "${mvnCmd} clean install -DskipTests=true"
   }

   stage 'Test and Analysis'
   dir ('source') {
		echo "\n\n\n*** Running tests ***"
		sh "${mvnCmd} test"
		step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*.xml'])

		echo "\n\n\n*** Running analysis ***"
		sh "${mvnCmd} jacoco:report sonar:sonar -Dsonar.host.url=http://sonarqube:9000 -DskipTests=true"
   }

   stage 'Push to Nexus'
   dir ('source') {
		sh "${mvnCmd} deploy -DskipTests=true"
   }

   stage 'Deploy DEV'
   sh "${ocCmd} start-build java1 --from-file=source/target/hello-${v}.jar --wait=true -n dev"

   stage 'Deploy STAGE'
   input message: "Promote to STAGE?", ok: "Promote"
   sh "${ocCmd} tag dev/java1:latest stage/java1:${v} -n stage"

   sh "curl -s -L -O https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64 && chmod a+x ./jq-linux64 && mv ./jq-linux64 ./jq"
   def jqFilter = ".spec.triggers[1].imageChangeParams.from.name = \"java1:${v}\""
   sh "${ocCmd} export dc java1 --output=json -n stage | ./jq '${jqFilter}' | ${ocCmd} replace dc java1 -n stage -f -"

}

def version() {
	def matcher = readFile('source/pom.xml') =~ '<version>(.+)</version>'
	matcher ? matcher[1][1] : null
}
...


# Install the Jenkins build root token plugin:
https://wiki.jenkins-ci.org/display/JENKINS/Build+Token+Root+Plugin


# Set token for job in Jenkins to: mycoolpassword

# Install gogs webhook in gogs
http://jenkins/buildByToken/build?job=s2i-dydx-java&token=mycoolpassword

If required disable cert checks for web hooks inside the gogs image:

/data/gogs/conf # cat app.ini 

...
[webhook] 
SKIP_TLS_VERIFY = true
...
