FROM openshift/base-centos7

ENV MAVEN_VERSION=3.3.9

LABEL io.k8s.description="Platform for building and running Java applications" \
      io.k8s.display-name="Java S2I builder" \
      io.openshift.expose-services="8080:http" \
      io.openshift.s2i.scripts-url=image:///usr/local/s2i \
      io.openshift.tags="builder,java"

RUN INSTALL_PKGS="tar unzip bc which lsof java-1.8.0-openjdk java-1.8.0-openjdk-devel" && \
    yum install -y --enablerepo=centosplus $INSTALL_PKGS && \
    rpm -V $INSTALL_PKGS && \
    yum clean all -y && \
    (curl -v https://www.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz | \
    tar -zx -C /usr/local) && \
    ln -sf /usr/local/apache-maven-$MAVEN_VERSION/bin/mvn /usr/local/bin/mvn && \
    mkdir -p $HOME/data

ADD source/pom.xml $HOME/
ADD localm2 $HOME/localm2
RUN mvn clean install --global-settings=$HOME/localm2/settings.xml --fail-never && rm -rf $HOME/build pom.xml

ADD root /

COPY ./.s2i/bin/ /usr/local/s2i

RUN chown -R 1001:0 $HOME

VOLUME ["$HOME/data"]

USER 1001

EXPOSE 8080

ENTRYPOINT ["container-entrypoint"]
CMD ["start-container.sh"]
