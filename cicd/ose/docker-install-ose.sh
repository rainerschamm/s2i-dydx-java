#!/bin/bash

sudo mkdir -p /var/lib/origin/openshift.local.config
sudo mkdir -p /var/lib/origin/openshift.local.volumes
sudo mkdir -p /var/lib/origin/openshift.local.etcd

sudo mount -t tmpfs -o size=1024m tmpfs /var/lib/origin/openshift.local.volumes
sudo mount --make-shared /var/lib/origin/openshift.local.volumes

sudo mount -t tmpfs -o size=256m tmpfs /var/lib/origin/openshift.local.etcd
sudo mount --make-shared /var/lib/origin/openshift.local.etcd

docker pull openshift/origin-sti-builder:v1.3.1
docker pull openshift/origin-deployer:v1.3.1
docker pull openshift/origin-docker-registry:v1.3.1
docker pull openshift/origin-haproxy-router:v1.3.1
docker pull openshift/origin:v1.3.1
docker pull openshift/origin-pod:v1.3.1

docker run -it \
	-e OPENSHIFT_CONTAINERIZED=false \
	-e HOME=/root \
	-e KUBECONFIG=/var/lib/origin/openshift.local.config/master/admin.kubeconfig \
	-e PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
	--privileged --pid=host --net=host \
	-v /var/run:/var/run:rw \
	-v /var/log:/var/log:rw \
	-v /sys:/sys:ro \
	-v /var/lib/docker:/var/lib/docker \
	-v /var/lib/origin/openshift.local.config:/var/lib/origin/openshift.local.config:z \
	openshift/origin:v1.3.1 start \
	--write-config=/var/lib/origin/openshift.local.config

# Or if u have the binary then:
openshift start --write-config=/var/lib/origin/openshift.local.config

Fix volumeDirectory in: /var/lib/origin/openshift.local.config/node-storm/node-config.yaml
to: /var/lib/origin/openshift.local.volumes

Fix storageDirectory in: /var/lib/origin/openshift.local.config/master/master-config.yaml
to: /var/lib/origin/openshift.local.etcd

Make sure the docker daemon mount flags are:
MountFlags=shared
(/lib/systemd/system/docker.service)

docker run -d -p 53:53 -p 8443:8443 \
	-e OPENSHIFT_CONTAINERIZED=false \
	-e HOME=/root \
	-e KUBECONFIG=/var/lib/origin/openshift.local.config/master/admin.kubeconfig \
	-e PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin \
	--privileged --pid=host --net=host \
	-v /var/run:/var/run:rw \
	-v /var/log:/var/log:rw \
	-v /sys:/sys:ro \
	-v /var/lib/docker:/var/lib/docker \
	-v /var/lib/origin/openshift.local.volumes:/var/lib/origin/openshift.local.volumes:shared \
	-v /var/lib/origin/openshift.local.etcd:/var/lib/origin/openshift.local.etcd:shared \
	-v /var/lib/origin/openshift.local.config:/var/lib/origin/openshift.local.config:z \
	openshift/origin:v1.3.1 start \
	--master-config=/var/lib/origin/openshift.local.config/master/master-config.yaml \
	--node-config=/var/lib/origin/openshift.local.config/node-storm/node-config.yaml

docker exec -it $(docker ps | grep "openshift/origin:" | awk '{print $1}') /bin/bash

oc login -u system:admin
oc adm policy add-cluster-role-to-user cluster-admin admin 
oc adm policy add-cluster-role-to-group cluster-reader system:serviceaccounts

oadm registry --config=/var/lib/origin/openshift.local.config/master/admin.kubeconfig --service-account=registry

oadm policy add-scc-to-user hostnetwork system:serviceaccount:default:router
oadm policy add-cluster-role-to-user cluster-reader system:serviceaccount:default:router
oadm router router --replicas=1 --service-account=router

oc project default
oc get svc
# Look for the docker registry and add something like this to your docker daemon options:

--insecure-registry http://172.30.36.165:5000 --insecure-registry https://172.30.36.165:5000 --insecure-registry 172.30.36.165:5000

# restart docker
sudo systemctl restart docker.service

# and start origin backup again using the same docker run command

# make sure u can log into the registry
oc login https://127.0.0.1:8443
oc project default
OSE_DOCKER_REG=`oc get svc -n default | grep docker-registry | awk '{print $2}'`
docker login --username=admin --password=$(oc whoami -t) $OSE_DOCKER_REG:5000
