#!/bin/bash

CONTAINER_IMAGE=s2i-dydx-java
CONTAINER_HOME=/opt/app-root/src
ARTIFACT=hello-1.0-SNAPSHOT.jar

function runBuild ()
{
	PROJECTFOLDER="$1"
	DIR="$( cd "$PROJECTFOLDER" && pwd )"

	CONTAINER_NAME=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1`

	echo "Creating new build container: $CONTAINER_NAME"
	docker run -v $DIR:$CONTAINER_HOME/data --name=$CONTAINER_NAME $CONTAINER_IMAGE
	docker cp $CONTAINER_NAME:$CONTAINER_HOME/build/$ARTIFACT .
	docker rm $CONTAINER_NAME
	echo "Removed build container: $CONTAINER_NAME"
}

#
# Check arguments and run the build
#
if [ ! $# -eq 1 ]
then
   BIN=`basename $0`
   echo "Usage: ./$BIN <maven_project>"
   exit 1
fi

if [ -d "$1" ] ; then
   runBuild "$1"
else
   echo "The maven project folder does not exist"
   BIN=`basename $0`
   echo "Usage: ./$BIN <maven_project>"
   exit 1
fi
