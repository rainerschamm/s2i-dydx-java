#!/bin/bash

CURRENT_DIR="$(readlink -f $(dirname $(readlink -f $0)))"
[ -r $CURRENT_DIR/ose-config.sh ] && . $CURRENT_DIR/ose-config.sh
[ -r $CURRENT_DIR/ose-common.sh ] && . $CURRENT_DIR/ose-common.sh

JQ_FILTER=".spec.triggers[1].imageChangeParams.from.name = \"$OSE_APP_IMAGE:$PROMOTED_VERSION\""

oc tag $OSE_PROJECT/$OSE_APP_IMAGE:latest $PROMOTED_PROJECT/$OSE_APP_IMAGE:$PROMOTED_VERSION
oc export dc $OSE_APP_IMAGE --output=json -n $PROMOTED_PROJECT | jq "$JQ_FILTER" | oc replace dc $OSE_APP_IMAGE -n $PROMOTED_PROJECT -f -
