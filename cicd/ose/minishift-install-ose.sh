1) Setup kvm driver
-------------------

$ sudo curl -L https://github.com/dhiltgen/docker-machine-kvm/releases/download/v0.7.0/docker-machine-driver-kvm -o /usr/local/bin/docker-machine-driver-kvm
$ sudo chmod +x /usr/local/bin/docker-machine-driver-kvm

# Install libvirt and qemu-kvm on your system, e.g.
# Debian/Ubuntu
$ sudo apt install libvirt-bin qemu-kvm
# Fedora/CentOS/RHEL
$ sudo yum install libvirt-daemon-kvm kvm

# Add yourself to the libvirtd group (use libvirt group for rpm based distros) so you don't need to sudo
$ sudo usermod -a -G libvirtd $(whoami)

# Update your current session for the group change to take effect
$ newgrp libvirtd


2) Start minishift
------------------

minishift start --vm-driver=kvm --memory=6144 --cpus=1 --disk-size=50g
minishift ssh

docker exec -it $(docker ps | grep "openshift/origin:" | awk '{print $1}') /bin/bash

oc login -u system:admin
oc adm policy add-cluster-role-to-user cluster-admin admin 
oc adm policy add-cluster-role-to-group cluster-reader system:serviceaccounts

oadm registry --config=/var/lib/origin/openshift.local.config/master/admin.kubeconfig --service-account=registry

oadm policy add-scc-to-user hostnetwork system:serviceaccount:default:router
oadm policy add-cluster-role-to-user cluster-reader system:serviceaccount:default:router
oadm router router --replicas=1 --service-account=router

#
# Manually start ose container:
#
sudo docker run -d -p 53:53 -p 8443:8443 -e OPENSHIFT_CONTAINERIZED=false -e HOME=/root -e KUBECONFIG=/var/lib/origin/openshift.local.config/master/admin.kubeconfig -e PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin --name "origin" --privileged --pid=host --net=host -v /var/run:/var/run:rw -v /sys:/sys:ro -v /var/lib/docker:/var/lib/docker -v /var/lib/origin/openshift.local.volumes:/var/lib/origin/openshift.local.volumes:shared -v /var/lib/origin/openshift.local.config:/var/lib/origin/openshift.local.config:z openshift/origin:v1.3.1 start --master-config=/var/lib/origin/openshift.local.config/master/master-config.yaml --node-config=/var/lib/origin/openshift.local.config/node-192.168.42.72/node-config.yaml
