#!/bin/bash

oc get pods | grep Completed | awk '{print $1}' | xargs -i oc delete pod {}
oc get pods | grep Error | awk '{print $1}' | xargs -i oc delete pod {}

