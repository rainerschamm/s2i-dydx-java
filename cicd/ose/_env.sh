#!/bin/bash

export KUBECONFIG="$(pwd)"/openshift.local.config/master/admin.kubeconfig
export CURL_CA_BUNDLE="$(pwd)"/openshift.local.config/master/ca.crt
export PATH="$(pwd)":$PATH

echo export KUBECONFIG=$KUBECONFIG
echo export CURL_CA_BUNDLE=$CURL_CA_BUNDLE
echo export PATH=$PATH
