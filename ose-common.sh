#!/bin/bash

function oseStartBuild()
{
	oc start-build $OSE_APP_IMAGE --from-dir=source --wait=true -n $OSE_PROJECT
}

function osePushNewBuilder()
{
	OSE_DOCKER_REG=`oc get svc -n default | grep docker-registry | awk '{print $2}'`
	docker login --username=admin --password=$(oc whoami -t) $OSE_DOCKER_REG:5000
	docker tag $OSE_BUILDER_IMAGE $OSE_DOCKER_REG:5000/openshift/$OSE_BUILDER_IMAGE
	docker push $OSE_DOCKER_REG:5000/openshift/$OSE_BUILDER_IMAGE
}

function oseNewBuild()
{
	oc new-build --name=$OSE_APP_IMAGE --image-stream=$OSE_BUILDER_IMAGE --binary=true --labels=app=$OSE_APP_IMAGE -n $OSE_PROJECT || true
}

function oseNewApp()
{
	oc new-app $OSE_APP_IMAGE:latest -n $OSE_PROJECT
	oc expose svc/$OSE_APP_IMAGE -n $OSE_PROJECT
}
